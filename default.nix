{ config, exports, home, lib, pkgs, ... }:
let

  inherit (config) domain;
  inherit (lib) mkOption;
  inherit (lib.types) enum int str;
  rainloop = pkgs.rainloop-community;

  # FIXME: this data dir hides config files
  dataDir = "${home}/data";

  leDir = "${home}/letsencrypt";
  certificatePath = "${leDir}/fullchain.pem";
  keyPath = "${leDir}/key.pem";

  nginxConfigLink = "${home}/nginx.conf";
  nginxLogDir = "${home}/nginx-logs";

  nginxConfig = pkgs.writeText "rainloop-nginx.conf" ''

    server {

      ${exports.nginx.listenDualExtras 443 ["ssl" "http2"]}
      ${exports.nginx.staticFilesConfig}
      ${exports.nginx.hstsConfig}

      server_name          ${domain};
      root                 ${rainloop};
      client_max_body_size 10M;

      access_log           ${nginxLogDir}/access.log bunyan;
      error_log            ${nginxLogDir}/error.log;

      ssl_certificate      ${certificatePath};
      ssl_certificate_key  ${keyPath};

      location ~ \.php$ {
        try_files     $uri = 404;
        include       ${pkgs.nginx}/conf/fastcgi_params;
        fastcgi_pass  127.0.0.1:${toString exports.php-fpm.port};
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
      }

      location / {
        index     index.php;
        try_files $uri $uri/ /index.php?$args;
      }
    }

    server {

      ${exports.nginx.listenDual 80}
      server_name ${domain};

      location ~ /.well-known {
        root ${leDir};
      }

      location / {
        return 301 https://${domain}$request_uri;
      }
    }
  '';

  hooks = pkgs.leUtils.renewHooks exports domain ++ [
    exports.nginx.reloadCommand
  ];

  # TODO: there must be a better name for this
  secureOptions = ["None" "SSL" "TLS"];

  domainsDir =
  let
    makeIni = name: value: pkgs.writeText "${name}.ini" ''
      imap_host = "${value.imapHost}"
      imap_port = ${toString value.imapPort}
      imap_secure = "${value.imapSecure}"
      smtp_host = "${value.smtpHost}"
      smtp_port = ${toString value.smtpPort}
      smtp_secure = "${value.smtpSecure}"
    '';
    links = lib.concatStrings (lib.mapAttrsToList (name: value: ''
      ln --symbolic ${makeIni name value} $out/${name}.ini
    '') config.mail-domains);
  in pkgs.runCommand "domains" {} ''
    mkdir $out
    ${links}
  '';

in {

  options = {
    domain = mkOption {
      type = str;
    };

    mail-domains = mkOption {
      type = lib.types.attrsOf (lib.types.submodule (
        { name, ... }:
        {
          options = {
            imapHost = mkOption {
              type = str;
              default = "imap.${name}";
            };
            imapPort = mkOption {
              type = int;
              default = 993;
            };
            imapSecure = mkOption {
              type = enum secureOptions;
              default = "SSL";
            };
            smtpHost = mkOption {
              type = str;
              default = "smtp.${name}";
            };
            smtpPort = mkOption {
              type = int;
              default = 465;
            };
            smtpSecure = mkOption {
              type = enum secureOptions;
              default = "SSL";
            };
          };
        }
      ));
    };
  };

  exports = {
    letsencrypt.certs.${domain} = leDir;
    nginx.http-include = nginxConfigLink;
  };

  run = pkgs.writeBash "rainloop" ''

    PATH=${lib.makeBinPath [
      pkgs.coreutils
    ]}

    mkdir --parents ${dataDir} ${leDir} ${nginxLogDir}
    chown www-data ${dataDir}
    ln --force --no-target-directory --symbolic ${domainsDir} ${dataDir}/_data_/_default_/domains
    ${pkgs.findutils}/bin/find ${dataDir}/_data_/_default_/cache -type f -delete

    [[ -e ${certificatePath} ]] || openssl req -x509 -nodes -batch \
      -newkey rsa \
      -out ${certificatePath} \
      -keyout ${keyPath}

    ln --force --symbolic ${nginxConfig} ${nginxConfigLink}
    ${exports.nginx.reloadCommand}

    exec ${pkgs.leUtils.renew [domain] leDir hooks}
  '';
}
